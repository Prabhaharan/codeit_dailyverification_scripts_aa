<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>UpdateUserPageIcon</name>
   <tag></tag>
   <elementGuidId>b304e372-5258-4d7c-80b4-86f828847892</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@eventproxy=&quot;AdminUpdateUser&quot; and @role=&quot;button&quot;]</value>
   </webElementProperties>
</WebElementEntity>
