<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ToDoVerify</name>
   <tag></tag>
   <elementGuidId>fb82d459-65ed-4641-a984-72dcc238b042</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()='To Do']/../following::*/div[contains(@class,'task-item-text')]/h3[text()]</value>
   </webElementProperties>
</WebElementEntity>
