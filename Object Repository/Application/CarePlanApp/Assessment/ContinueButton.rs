<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ContinueButton</name>
   <tag></tag>
   <elementGuidId>6117b2bb-480a-4cf5-847c-5cd1a6da9739</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@value=&quot;Continue -->&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@value=&quot;Continue -->&quot;]</value>
   </webElementProperties>
</WebElementEntity>
