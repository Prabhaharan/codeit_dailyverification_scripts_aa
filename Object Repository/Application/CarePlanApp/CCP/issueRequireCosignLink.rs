<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>issueRequireCosignLink</name>
   <tag></tag>
   <elementGuidId>20f0c1bc-4339-4b45-8481-87b09ecd97cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[text()='CCP Notifications']/following::span[@class='notificationsLinkWarn']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[text()='CCP Notifications']/following::span[@class='notificationsLinkWarn']</value>
   </webElementProperties>
</WebElementEntity>
