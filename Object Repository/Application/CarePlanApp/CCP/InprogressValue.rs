<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>InprogressValue</name>
   <tag></tag>
   <elementGuidId>ce518167-763a-4a5f-b620-c2e5aa4a09aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//option[text()='In Progress']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//option[text()='In Progress']</value>
   </webElementProperties>
</WebElementEntity>
