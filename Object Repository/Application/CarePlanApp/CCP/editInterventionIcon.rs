<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>editInterventionIcon</name>
   <tag></tag>
   <elementGuidId>aa4a4ccc-b633-4fdb-a8c1-a651c570e362</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//img[@title='Edit this intervention']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//img[@title='Edit this intervention']</value>
   </webElementProperties>
</WebElementEntity>
