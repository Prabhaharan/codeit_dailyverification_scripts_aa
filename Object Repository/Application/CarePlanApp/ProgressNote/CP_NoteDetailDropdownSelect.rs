<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_NoteDetailDropdownSelect</name>
   <tag></tag>
   <elementGuidId>95c00f48-493a-4a1d-9c93-b07499abb0cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[text()[normalize-space() ='Note Detail:']]//following-sibling::td/table/tbody/tr/td/select</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()[normalize-space() ='Note Detail:']]//following-sibling::td/table/tbody/tr/td/select</value>
   </webElementProperties>
</WebElementEntity>
