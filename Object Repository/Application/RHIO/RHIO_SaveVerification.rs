<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RHIO_SaveVerification</name>
   <tag></tag>
   <elementGuidId>2022a4ec-576e-4b08-abe1-f47a7eaa6ca7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//gsi-save-md-button[@id='btnSaveRhio'][@aria-disabled='true']//span[text()='Save']</value>
   </webElementProperties>
</WebElementEntity>
