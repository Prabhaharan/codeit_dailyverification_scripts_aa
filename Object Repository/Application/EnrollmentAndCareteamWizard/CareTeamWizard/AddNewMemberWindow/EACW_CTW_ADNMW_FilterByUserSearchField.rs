<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_CTW_ADNMW_FilterByUserSearchField</name>
   <tag></tag>
   <elementGuidId>62b843a8-5bbf-44ab-af5d-8aa80dff713d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'searchCrossCommTxt']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>searchCrossCommTxt</value>
   </webElementProperties>
</WebElementEntity>
