<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_RemoveCareTeamButton</name>
   <tag></tag>
   <elementGuidId>f6082d8a-6504-4396-84b4-ad2ef34f1523</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[text()='Remove']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[text()='Remove']</value>
   </webElementProperties>
</WebElementEntity>
