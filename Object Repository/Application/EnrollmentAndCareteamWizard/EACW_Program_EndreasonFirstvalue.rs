<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_Program_EndreasonFirstvalue</name>
   <tag></tag>
   <elementGuidId>33ff45a9-f2fa-4406-95b8-c12ed1359e3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//md-option[@ng-repeat=&quot;opt in ctrl.data.program.endReasons | filter:endReasonFilter()&quot;][1] | //md-option[@ng-repeat=&quot;opt in ctrl.data.program.endReasons&quot;][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-option[@ng-repeat=&quot;opt in ctrl.data.program.endReasons | filter:endReasonFilter()&quot;][1] | //md-option[@ng-repeat=&quot;opt in ctrl.data.program.endReasons&quot;][1]</value>
   </webElementProperties>
</WebElementEntity>
