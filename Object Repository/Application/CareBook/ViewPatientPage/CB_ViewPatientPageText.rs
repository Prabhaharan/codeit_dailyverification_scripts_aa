<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CB_ViewPatientPageText</name>
   <tag></tag>
   <elementGuidId>b7814fa7-1b45-4cf0-9864-011d71e0b42d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h2[text()='Carebook Search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[text()='Carebook Search']</value>
   </webElementProperties>
</WebElementEntity>
