<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_Clear_FirstName</name>
   <tag></tag>
   <elementGuidId>d7a66621-3a54-46b5-87dc-9169a64634bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'First Name')]//parent::nobr/..//following-sibling::td/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'First Name')]//parent::nobr/..//following-sibling::td/input</value>
   </webElementProperties>
</WebElementEntity>
