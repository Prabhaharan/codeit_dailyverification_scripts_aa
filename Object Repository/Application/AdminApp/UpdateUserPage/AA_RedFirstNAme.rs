<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_RedFirstNAme</name>
   <tag></tag>
   <elementGuidId>86b50f94-a968-4814-95f0-b511fec7755b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'First Name')]//parent::nobr/..//following-sibling::td//following::input[@name=&quot;isc_TextItem_10&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'First Name')]//parent::nobr/..//following-sibling::td//following::input[@name=&quot;isc_TextItem_10&quot;]</value>
   </webElementProperties>
</WebElementEntity>
