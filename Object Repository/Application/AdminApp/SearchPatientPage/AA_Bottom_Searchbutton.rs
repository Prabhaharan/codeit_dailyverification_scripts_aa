<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_Bottom_Searchbutton</name>
   <tag></tag>
   <elementGuidId>907b646a-419f-4292-900f-686a09980bb1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[normalize-space(text()) ='Search'])[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[normalize-space(text()) ='Search'])[3]</value>
   </webElementProperties>
</WebElementEntity>
