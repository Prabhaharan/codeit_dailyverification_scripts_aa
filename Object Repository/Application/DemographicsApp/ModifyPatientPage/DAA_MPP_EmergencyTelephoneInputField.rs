<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DAA_MPP_EmergencyTelephoneInputField</name>
   <tag></tag>
   <elementGuidId>982fbdba-b5d1-4239-9a24-1cfbe9ee012d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'newTextItem_1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id=&quot;isc_G0&quot;]</value>
   </webElementProperties>
</WebElementEntity>
