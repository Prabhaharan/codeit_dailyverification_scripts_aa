<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_MPP_SaveSuccessfullPopupOkButton</name>
   <tag></tag>
   <elementGuidId>88927f3c-162b-44aa-951b-33f62021f2e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()='Save successful.']//following::div[text()='OK']</value>
   </webElementProperties>
</WebElementEntity>
