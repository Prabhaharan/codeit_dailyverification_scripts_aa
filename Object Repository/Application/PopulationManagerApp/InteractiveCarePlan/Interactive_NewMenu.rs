<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Interactive_NewMenu</name>
   <tag></tag>
   <elementGuidId>3b55f156-6b0a-4ff7-8f81-e735d8290e89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[@class=&quot;pentaho-page-background dragdrop-dropTarget dragdrop-boundary&quot;]/div[@class=&quot;gwt-MenuBarPopup&quot;]/div[1]/table[1]/tbody[1]/tr[@class=&quot;menuPopupMiddle&quot;]/td[@class=&quot;menuPopupMiddleCenter&quot;]/div[@class=&quot;menuPopupMiddleCenterInner menuPopupContent&quot;]/div[@id=&quot;filemenu&quot;]/table[1]/tbody[1]/tr[1]/td[@id=&quot;newmenu&quot;] </value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;pentaho-page-background dragdrop-dropTarget dragdrop-boundary&quot;]/div[@class=&quot;gwt-MenuBarPopup&quot;]/div[1]/table[1]/tbody[1]/tr[@class=&quot;menuPopupMiddle&quot;]/td[@class=&quot;menuPopupMiddleCenter&quot;]/div[@class=&quot;menuPopupMiddleCenterInner menuPopupContent&quot;]/div[@id=&quot;filemenu&quot;]/table[1]/tbody[1]/tr[1]/td[@id=&quot;newmenu&quot;] </value>
   </webElementProperties>
</WebElementEntity>
