<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Parent_Program_dropdown</name>
   <tag></tag>
   <elementGuidId>b0585c3d-ffcd-402b-a6b3-95cfaa4bed05</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Parent Program']/following::select[1]</value>
   </webElementProperties>
</WebElementEntity>
