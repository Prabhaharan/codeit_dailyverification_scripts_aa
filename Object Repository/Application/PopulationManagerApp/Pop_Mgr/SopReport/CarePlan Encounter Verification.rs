<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CarePlan Encounter Verification</name>
   <tag></tag>
   <elementGuidId>8ca38b8d-63ed-41a1-93e8-81a462b1bcce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//td[text()='Patient ID'][@valign='middle']/../following-sibling::tr[1]/td)[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//td[text()='Patient ID'][@valign='middle']/../following-sibling::tr[1]/td)[1]</value>
   </webElementProperties>
</WebElementEntity>
