<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Assessment Verification</name>
   <tag></tag>
   <elementGuidId>5ef52b44-b2a2-4a58-a90a-e3519b8114cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//td[text()='Patient ID'][@valign='middle']/../following-sibling::tr[1]/td)[1]</value>
   </webElementProperties>
</WebElementEntity>
