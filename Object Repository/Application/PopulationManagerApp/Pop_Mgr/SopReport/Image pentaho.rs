<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Image pentaho</name>
   <tag></tag>
   <elementGuidId>3b304284-645d-464b-b300-9ef61fc8b51c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='pentaho-tab-bar']//div[contains(text(),'Report')]/following::img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://qa.gsihealth.net/pentaho/content/common-ui/resources/themes/images/spacer.gif</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>gwt-Image pentaho-tabWidget-close pentaho-closebutton pentaho-imagebutton-hover</value>
   </webElementProperties>
</WebElementEntity>
