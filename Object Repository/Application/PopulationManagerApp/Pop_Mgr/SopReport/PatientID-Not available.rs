<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PatientID-Not available</name>
   <tag></tag>
   <elementGuidId>4cfb8e32-a94e-4003-a29e-ca1caffd41ec</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Not Available']//following::tr/td</value>
   </webElementProperties>
</WebElementEntity>
