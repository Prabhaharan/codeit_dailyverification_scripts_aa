<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Program Enrollment Verification</name>
   <tag></tag>
   <elementGuidId>56530d95-2855-433f-9ef5-7a54fbc5c518</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//td[text()='Patient ID'][@valign='middle']/../following-sibling::tr[1]/td)[1]</value>
   </webElementProperties>
</WebElementEntity>
