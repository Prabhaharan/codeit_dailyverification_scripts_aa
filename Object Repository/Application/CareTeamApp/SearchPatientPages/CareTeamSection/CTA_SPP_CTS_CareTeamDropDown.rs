<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CTA_SPP_CTS_CareTeamDropDown</name>
   <tag></tag>
   <elementGuidId>2644063a-c849-4f09-8e99-8709a661fe91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;Care Team&quot;)]/..//following-sibling::td/table/tbody/tr/td[@class=&quot;comboBoxItemPickerCell&quot;]</value>
   </webElementProperties>
</WebElementEntity>
