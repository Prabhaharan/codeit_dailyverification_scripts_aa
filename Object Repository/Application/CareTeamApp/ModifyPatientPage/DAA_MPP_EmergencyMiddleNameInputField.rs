<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DAA_MPP_EmergencyMiddleNameInputField</name>
   <tag></tag>
   <elementGuidId>007a4e48-acc0-4790-b022-da8ed2872590</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'mNameEmergencyTextItem']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>mNameEmergencyTextItem</value>
   </webElementProperties>
</WebElementEntity>
