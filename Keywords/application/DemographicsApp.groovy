package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import keywordsLibrary.CommomLibrary
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class DemographicsApp {
	@Keyword
	public Boolean DemographcisAppNavigation(){
		try{
			WebUI.verifyElementPresent(findTestObject('Application/DemographicsApp/AppDemographicIcon'), 5, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/DemographicsApp/AppDemographicIcon'), FailureHandling.STOP_ON_FAILURE)
			Boolean appNavigationStatus=WebUI.verifyElementPresent(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_searchButton'), 5, FailureHandling.OPTIONAL)
			return appNavigationStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean searchPatientInDemographcisAppwithLN(String patientID,String patientLastName){
		try{
			//Search patient with PatientLastName and Patient ID
			//WebUI.click(findTestObject('Object Repository/Application/DemographicsApp/SearchPatientPage/DA_ClearButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(4)
			TestObject patientSearchObject = findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("//div[text()='" +patientID+"']//following::td/div[text()='"+patientLastName+"']")
			Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject, 30, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return patietSearchStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean searchPatientInDemographcisAppwithPatientID(String patientID){
		try{
			//Search patient with PatientLastName and Patient ID
			WebUI.click(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_searchButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(4)
			TestObject patientSearchObject = findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("//div[text()='" +patientID+"']")
			Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return patietSearchStatus
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean searchPatientInDemographcisAppwithPatientIDOne(String patientID1){
		try{
			//Search patient with PatientLastName and Patient ID
			WebUI.click(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_searchButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(4)
			TestObject patientSearchObject = findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("//div[text()='" +patientID1+"']")
			Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return patietSearchStatus
		} catch(Exception e){
			return false
		}
	}

	@Keyword
	public Boolean searchPatientInDemographcisAppwithFN(String patientID,String firstName){
		try{
			//Search patient with PatientLastName and Patient ID
			WebUI.click(findTestObject('Application/DemographicsApp/SearchPatientPage/DA_SPP_searchButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(4)
			TestObject patientSearchObject = findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("//div[text()='" + patientID+"']/following::td/div[text()='"+firstName+"']")
			Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return patietSearchStatus
		} catch(Exception e){
			return false
		}
	}

	@Keyword
	public Boolean searchPatientInDemographcisApp(String patientID, String patientLastName){
		try{
			//Search patient with PatientLastName and Patient ID
			WebUiCommonHelper.findWebElement(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_PatientLastNameInputField'), 5).clear()
			WebUI.setText(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_PatientLastNameInputField'), patientLastName, FailureHandling.STOP_ON_FAILURE)
			WebUiCommonHelper.findWebElement(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_PatientIDinputField'), 5).clear()
			WebUI.delay(1)
			WebUI.setText(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_PatientIDinputField'), patientID, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(1)
			WebUI.click(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_searchButton'), FailureHandling.STOP_ON_FAILURE)
			TestObject patientSearchObject = findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("//div[text()='" + patientID+"']/following::td/div[text()='"+patientLastName+"']")
			Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return patietSearchStatus
		} catch(Exception e){
			return false
		}
	}

	@Keyword
	public Boolean selectPatientInDemographcisApp(){
		try{
			//Select patient with PatientLastName and Patient ID
			TestObject patientSearchObject = findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("//div[text()='" + patientID+"']/following::td/div[text()='"+lastName+"']")
			Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return patietSearchStatus
		} catch(Exception e){
			return false
		}
	}

	@Keyword
	public Boolean searchPatientInDemographcisApp(String patientLastName){
		try{
			//Search patient with PatientLastName and Patient ID
			WebUiCommonHelper.findWebElement(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_PatientLastNameInputField'), 5).clear()
			WebUI.setText(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_PatientLastNameInputField'), patientLastName, FailureHandling.STOP_ON_FAILURE)
			//WebUiCommonHelper.findWebElement(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_PatientIDinputField'), 5).clear()
			//WebUI.delay(1)
			//WebUI.setText(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_PatientIDinputField'), patientID, FailureHandling.STOP_ON_FAILURE)
			//WebUI.delay(1)
			WebUI.click(findTestObject('Applications/DemographicsApp/SearchPatientPage/DA_SPP_searchButton'), FailureHandling.STOP_ON_FAILURE)
			TestObject patientSearchObject = findTestObject('Base/commanXpath')
			patientSearchObject.findProperty('xpath').setValue("//div[text()='"+patientLastName+"']")
			Boolean patietSearchStatus=WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
			WebUI.delay(3)
			WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			return patietSearchStatus
		} catch(Exception e){
			return false
		}
	}





	@Keyword
	public Boolean searchPatientInDemographcisAppwithdifferentField(Map<String, String> verifyfields){
		CommomLibrary commomLibrary=new CommomLibrary()
		try{
			for(def fieldName : verifyfields.keySet()){
				switch(fieldName){

					case "patientID":

						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"

						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(5)
						break
					case "lastName":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "firstName":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "middleName":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "dateOfBirth":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "gender":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "SSN":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "address1":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "address2":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "city":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "state":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "zipcode":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "telephone":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "parentName":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "programName":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "programEnrollmentConsent":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)

						break
					case "patientStatus":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "insurance":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
					case "organization":
						def fieldXpath ="//div[@id='tabappEnrollment']//tr[@role='listitem']//div[text()='"+verifyfields.get(fieldName)+"']"
						WebUI.verifyElementPresent(commomLibrary.dynamicElement(fieldXpath),10,FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
						break
				}
			}
			return true
		}catch(Exception e){
			return false
		}
	}
}
