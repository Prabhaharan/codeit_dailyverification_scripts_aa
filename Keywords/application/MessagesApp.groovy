package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import internal.GlobalVariable
import keywordsLibrary.CommomLibrary
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI



public class MessagesApp {

	@Keyword
	public Boolean MessagesAppNavigation(){
		try{
			WebUI.click(findTestObject('Application/Messages/AppMessagesicon'),FailureHandling.STOP_ON_FAILURE)
			WebUI.switchToWindowIndex(1)
			WebUI.maximizeWindow()
			WebUI.delay(5)
			Boolean MessagesAppNavigation=WebUI.verifyElementPresent(findTestObject('Application/Messages/HomePage/MS_HP_NewButton'), 15, FailureHandling.OPTIONAL)
			return MessagesAppNavigation
		} catch(Exception e){
			return false
		}
	}


	@Keyword
	public  Boolean VerifyInboxEmail(String mailSubject){
		Boolean Status=false
		try{
			CommomLibrary commomLibrary=new CommomLibrary()

			//	def mailSubject = commomLibrary.randomText()+commomLibrary.getRandomNumber()
			//Verify mail in receiver(TO) Inbox
			WebUI.click(findTestObject('Application/Messages/HomePage/MS_HP_FolderInbox'),FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			WebUI.click(findTestObject('Application/Messages/HomePage/MS_HP_RefreshButton'),FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(3)
			//Verify Mail received in receiver(TO) Inbox
			for(int refreshCount=1;refreshCount<=6;refreshCount++){
				WebUI.click(findTestObject('Application/Messages/HomePage/MS_HP_RefreshButton'),FailureHandling.STOP_ON_FAILURE)
				Status =WebUI.verifyElementPresent(commomLibrary.dynamicElement("//td[text()='"+mailSubject+"']"),10, FailureHandling.OPTIONAL)
				if(Status){
					break;
				}

			}
		} catch(Exception e){
			return false
		}
		return Status

	}

	@Keyword
	public  Boolean SearchEmail(String textValue){
		Boolean Status=false
		try{
			WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/Messages/HomePage/MS_HP_SearchInputField'), 5).clear()
			WebUI.setText(findTestObject('Object Repository/Application/Messages/HomePage/MS_HP_SearchInputField'),textValue, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Object Repository/Application/Messages/HomePage/MS_HP_SearchButton'),FailureHandling.STOP_ON_FAILURE)
			TestObject SearchXpath=findTestObject('Base/commanXpath')
			SearchXpath.findProperty('xpath').setValue("//td[text()='"+textValue+"']")

			Boolean SearchValue=WebUI.verifyElementPresent(SearchXpath, 30, FailureHandling.OPTIONAL)
			return SearchValue

		}
		catch(Exception e){
			return false
		}
	}
}
