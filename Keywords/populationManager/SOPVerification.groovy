package populationManager

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class SOPVerification {
	@Keyword
	public Boolean alertReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Alerts Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean assessmentReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Assessment Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean btqDemographicReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/BTQ Demographic Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean btqEncounterActivityReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/BTQ Encounter Activity Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean carePlanEncounterReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/CarePlan Encounter Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean carePlanGapReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/CarePlan Gap Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean careTeamReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/CareTeam Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}

	@Keyword
	public Boolean healthixAlertsReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Healthix Alerts Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean issuesReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Issues Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean patientProgrammingInformationReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Patient Programming Information verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean patientEnrollmentReportPageVerification(){
		try{

			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Pop_Mgr/SopReport/Program Enrollment Verification'), 500, FailureHandling.STOP_ON_FAILURE)

			return true
		} catch(Exception e){
			return false
		}
	}
}