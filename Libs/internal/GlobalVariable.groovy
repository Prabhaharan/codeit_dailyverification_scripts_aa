package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object testCase
     
    /**
     * <p></p>
     */
    public static Object extentrep
     
    /**
     * <p></p>
     */
    public static Object testrep
     
    /**
     * <p></p>
     */
    public static Object runStatus
     
    /**
     * <p></p>
     */
    public static Object runSelect
     
    /**
     * <p></p>
     */
    public static Object testRun
     
    /**
     * <p></p>
     */
    public static Object urlData
     
    /**
     * <p></p>
     */
    public static Object passwordData
     
    /**
     * <p></p>
     */
    public static Object userIDData
     
    /**
     * <p></p>
     */
    public static Object ENVData
     
    /**
     * <p></p>
     */
    public static Object testSuite
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['testCase' : '', 'extentrep' : '', 'testrep' : '', 'runStatus' : '', 'runSelect' : '', 'testRun' : [], 'urlData' : '', 'passwordData' : '', 'userIDData' : '', 'ENVData' : '', 'testSuite' : ''])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        testCase = selectedVariables['testCase']
        extentrep = selectedVariables['extentrep']
        testrep = selectedVariables['testrep']
        runStatus = selectedVariables['runStatus']
        runSelect = selectedVariables['runSelect']
        testRun = selectedVariables['testRun']
        urlData = selectedVariables['urlData']
        passwordData = selectedVariables['passwordData']
        userIDData = selectedVariables['userIDData']
        ENVData = selectedVariables['ENVData']
        testSuite = selectedVariables['testSuite']
        
    }
}
