import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus;
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException

ExtentReports rep
ExtentTest test
def instance= GlobalVariable.extentrep
println instance
if(instance.equals("")){
	GlobalVariable.ENVData=ENV
	GlobalVariable.urlData =url
	GlobalVariable.userIDData =userID
	GlobalVariable.passwordData=password
	rep = CustomKeywords.'reports.extentReports.getInstance'()
	GlobalVariable.extentrep =rep
	println GlobalVariable.extentrep
	ExtentReports report = GlobalVariable.extentrep
	 test = report.startTest(GlobalVariable.testCase)
	GlobalVariable.testrep = test
	test.log(LogStatus.INFO,"Login to the Dashboard")
}
try{
WebUI.openBrowser(GlobalVariable.urlData, FailureHandling.STOP_ON_FAILURE)
//Login 
WebUI.delay(1)
WebUI.maximizeWindow()
WebUI.setViewPortSize(1920, 1080)
WebUI.delay(2)
WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/userID"), 10, FailureHandling.STOP_ON_FAILURE)
WebUI.setText(findTestObject("Object Repository/LoginPage/userID"), GlobalVariable.userIDData, FailureHandling.STOP_ON_FAILURE)
WebUI.scrollToElement(findTestObject("Object Repository/LoginPage/password"), 10)
WebUI.setText(findTestObject("Object Repository/LoginPage/password"), GlobalVariable.passwordData, FailureHandling.STOP_ON_FAILURE)
WebUI.scrollToElement(findTestObject("Object Repository/LoginPage/loginButton"), 10)
WebUI.click(findTestObject("Object Repository/LoginPage/loginButton"), FailureHandling.STOP_ON_FAILURE)
//Samsha
WebUI.delay(2)
if(WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/samshaPopupConfirmation"), 3, FailureHandling.OPTIONAL)){
	WebUI.click(findTestObject("Object Repository/LoginPage/samshaPopupConfirmation"), FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject("Object Repository/LoginPage/samshaPopupIAgreeButton"), FailureHandling.STOP_ON_FAILURE)

}
if(WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/PopUpGotIt"),10, FailureHandling.OPTIONAL)) {
	WebUI.click(findTestObject("Object Repository/LoginPage/PopUpGotIt"), FailureHandling.STOP_ON_FAILURE)

}

 GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
 test.log(LogStatus.PASS, GlobalVariable.testCase);
}catch(StepFailedException e){
	test.log(LogStatus.FAIL, e.message)
	CustomKeywords.'reports.extentReports.takeScreenshot'(test)
	throw e
}