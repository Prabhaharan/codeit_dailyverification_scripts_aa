import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.LogStatus
import com.relevantcodes.extentreports.ExtentTest
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException

ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false
		    test.log(LogStatus.INFO,"Navigate to Population manager app")
			// Navigate to population manager app
			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/Common/Pop_Mgr_icon'), 5, FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(2)
			WebUI.click(findTestObject('Application/PopulationManagerApp/Common/Pop_Mgr_icon'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(2)
			WebUI.switchToWindowIndex(1)
			WebUI.delay(2)
			WebUI.setViewPortSize(1920, 1080)
			WebUI.delay(4)
			
			//Navigate to Interactive Report
			test.log(LogStatus.INFO,"Selected Patient Search")
			WebUI.waitForElementNotPresent(findTestObject('Object Repository/Application/PopulationManagerApp/InteractivePatientsValidation/PM_Common_LoadingSymbol'), 150, FailureHandling.OPTIONAL)
			WebUI.waitForElementPresent(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_FileMenu'), 30, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_FileMenu'), FailureHandling.STOP_ON_FAILURE)
			WebUI.mouseOver(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_NewMenu'), FailureHandling.STOP_ON_FAILURE)
			WebUI.verifyElementPresent(findTestObject("Application/PopulationManagerApp/InteractiveCarePlan/InteractiveReport"), 5,FailureHandling.STOP_ON_FAILURE)
			WebUI.mouseOver(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/InteractiveReport'), FailureHandling.STOP_ON_FAILURE)
			
			WebUI.click(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/InteractiveReport'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(5)
			if(WebUI.verifyElementPresent(findTestObject("Object Repository/Application/PopulationManagerApp/InteractiveCarePlan/Get Started"), 5,FailureHandling.OPTIONAL)){
				WebUI.click(findTestObject("Object Repository/Application/PopulationManagerApp/InteractiveCarePlan/Get Started"),FailureHandling.OPTIONAL)
			}
			WebUI.delay(2)
			WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_Frame'), 5, FailureHandling.STOP_ON_FAILURE)
			WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_CarePlan_DataSources'), 15,FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(2)
			
			if(WebUI.verifyElementPresent(findTestObject("Application/PopulationManagerApp/InteractiveCarePlan/Interactive_AuditFeild_Verify"), 5, FailureHandling.OPTIONAL)){
				CustomKeywords.'populationManager.InteractivePatientsVerification.keypressUsingJS'()
					}
				CustomKeywords.'populationManager.InteractivePatientsVerification.DoubleClickUsingJS'(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_CarePlan_DataSources'), 15)
				WebUI.verifyElementPresent(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_CarePlan_Ok'), 15, FailureHandling.STOP_ON_FAILURE)
				CustomKeywords.'populationManager.InteractivePatientsVerification.clickUsingJS'(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_CarePlan_Ok'), 10)
				WebUI.switchToDefaultContent();
				WebUI.switchToFrame(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_Frame'), 5, FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(10)
				
				//verifying gender values
				WebUI.verifyElementPresent(findTestObject("Application/PopulationManagerApp/InteractiveCarePlan/Interactive_gender"), 5)
				CustomKeywords.'populationManager.InteractivePatientsVerification.DoubleClickUsingJS'(findTestObject('Application/PopulationManagerApp/InteractiveCarePlan/Interactive_gender'), 15)
				WebUI.delay(50)
				
				//verifying by getting text
				WebUI.verifyElementPresent(findTestObject("Object Repository/Application/PopulationManagerApp/InteractiveCarePlan/Interactive_Gender_Verify"), 250)
				def Gender =WebUI.getText(findTestObject("Object Repository/Application/PopulationManagerApp/InteractiveCarePlan/Interactive_Gender_Verify"))
				println(Gender)
			    
				
				WebUI.closeWindowIndex(1)
				WebUI.switchToDefaultContent()
			
	GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
	test.log(LogStatus.PASS,"TestCase is executed successfully")
	}
	}catch(StepFailedException e){
	test.log(LogStatus.FAIL, e.message)
	CustomKeywords.'reports.extentReports.takeScreenshot'(test)
	throw e
}